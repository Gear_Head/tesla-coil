#pragma once
#include <string>
#define help		0x00
#define run			0x01
#define restart		0x02
#define heat		0x03

namespace Project3 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	int count_up =0;
	
	
	/// <summary>
	/// Zusammenfassung f�r MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		
		MyForm(void)
		{
			
			InitializeComponent();
			//
			//TODO: Konstruktorcode hier hinzuf�gen.
			//string var = "hello world";
			//
		}

	protected:
		/// <summary>
		/// Verwendete Ressourcen bereinigen.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TextBox^ textBox1;
	protected:
	private: System::Windows::Forms::TextBox^ textBox2;
	private: System::Windows::Forms::Button^ Enter;
	private: System::Windows::Forms::Button^ left;
	private: System::Windows::Forms::Button^ right;



	private: System::Windows::Forms::TextBox^ textBox3;
	private: System::Windows::Forms::Button^ Not_aus;
	private: System::Windows::Forms::Button^ start;
	private: System::Windows::Forms::Button^ lm;
	private: System::Windows::Forms::Button^ sm;




	private: System::Windows::Forms::Button^ Back;


	private:
		/// <summary>
		/// Erforderliche Designervariable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Erforderliche Methode f�r die Designerunterst�tzung.
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor ge�ndert werden.
		/// </summary>
		void InitializeComponent(void)
		{
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->Enter = (gcnew System::Windows::Forms::Button());
			this->left = (gcnew System::Windows::Forms::Button());
			this->right = (gcnew System::Windows::Forms::Button());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->Not_aus = (gcnew System::Windows::Forms::Button());
			this->start = (gcnew System::Windows::Forms::Button());
			this->lm = (gcnew System::Windows::Forms::Button());
			this->sm = (gcnew System::Windows::Forms::Button());
			this->Back = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(369, 12);
			this->textBox1->Multiline = true;
			this->textBox1->Name = L"textBox1";
			this->textBox1->ReadOnly = true;
			this->textBox1->Size = System::Drawing::Size(381, 240);
			this->textBox1->TabIndex = 0;
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(369, 276);
			this->textBox2->Multiline = true;
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(269, 31);
			this->textBox2->TabIndex = 1;
			// 
			// Enter
			// 
			this->Enter->Location = System::Drawing::Point(675, 276);
			this->Enter->Name = L"Enter";
			this->Enter->Size = System::Drawing::Size(75, 31);
			this->Enter->TabIndex = 2;
			this->Enter->Text = L"Enter";
			this->Enter->UseVisualStyleBackColor = true;
			this->Enter->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
			// 
			// left
			// 
			this->left->Location = System::Drawing::Point(35, 451);
			this->left->Name = L"left";
			this->left->Size = System::Drawing::Size(95, 41);
			this->left->TabIndex = 3;
			this->left->Text = L"<<<";
			this->left->UseVisualStyleBackColor = true;
			this->left->Click += gcnew System::EventHandler(this, &MyForm::left_Click);
			// 
			// right
			// 
			this->right->Location = System::Drawing::Point(225, 451);
			this->right->Name = L"right";
			this->right->Size = System::Drawing::Size(95, 41);
			this->right->TabIndex = 4;
			this->right->Text = L">>>";
			this->right->UseVisualStyleBackColor = true;
			this->right->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::right_MouseClick);
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(35, 369);
			this->textBox3->Multiline = true;
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(285, 43);
			this->textBox3->TabIndex = 5;
			this->textBox3->TextChanged += gcnew System::EventHandler(this, &MyForm::textBox3_TextChanged);
			// 
			// Not_aus
			// 
			this->Not_aus->BackColor = System::Drawing::Color::Red;
			this->Not_aus->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 18, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->Not_aus->ForeColor = System::Drawing::SystemColors::ControlDarkDark;
			this->Not_aus->Location = System::Drawing::Point(369, 343);
			this->Not_aus->Name = L"Not_aus";
			this->Not_aus->Size = System::Drawing::Size(381, 162);
			this->Not_aus->TabIndex = 6;
			this->Not_aus->Text = L"Not-Aus";
			this->Not_aus->UseVisualStyleBackColor = false;
			// 
			// start
			// 
			this->start->BackColor = System::Drawing::Color::Lime;
			this->start->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.125F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->start->Location = System::Drawing::Point(35, 12);
			this->start->Name = L"start";
			this->start->Size = System::Drawing::Size(285, 68);
			this->start->TabIndex = 7;
			this->start->Text = L"Start";
			this->start->UseVisualStyleBackColor = false;
			// 
			// lm
			// 
			this->lm->Location = System::Drawing::Point(35, 98);
			this->lm->Name = L"lm";
			this->lm->Size = System::Drawing::Size(285, 68);
			this->lm->TabIndex = 8;
			this->lm->Text = L"Lightning-Mode";
			this->lm->UseVisualStyleBackColor = true;
			// 
			// sm
			// 
			this->sm->Location = System::Drawing::Point(35, 184);
			this->sm->Name = L"sm";
			this->sm->Size = System::Drawing::Size(285, 68);
			this->sm->TabIndex = 9;
			this->sm->Text = L"Singing-Mode";
			this->sm->UseVisualStyleBackColor = true;
			// 
			// Back
			// 
			this->Back->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(128)), static_cast<System::Int32>(static_cast<System::Byte>(255)),
				static_cast<System::Int32>(static_cast<System::Byte>(255)));
			this->Back->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.125F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->Back->Location = System::Drawing::Point(35, 276);
			this->Back->Name = L"Back";
			this->Back->Size = System::Drawing::Size(285, 68);
			this->Back->TabIndex = 10;
			this->Back->Text = L"Back";
			this->Back->UseVisualStyleBackColor = false;
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(12, 25);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(790, 523);
			this->Controls->Add(this->Back);
			this->Controls->Add(this->sm);
			this->Controls->Add(this->lm);
			this->Controls->Add(this->start);
			this->Controls->Add(this->Not_aus);
			this->Controls->Add(this->textBox3);
			this->Controls->Add(this->right);
			this->Controls->Add(this->left);
			this->Controls->Add(this->Enter);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->textBox1);
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e) {
		int value = Convert::ToInt32(textBox2->Text);
		switch (value)
		{
		case help:
			textBox1->Text = "help";
			///
			break;
		case heat:
			textBox1->Text = "heat";
			///
			break;
		case run:
			textBox1->Text = "run";
			break;
		case restart:
			textBox1->Text = "restart";
			break;
		default:
			textBox1->Text = "null";
			break;
		}

	}

private: System::Void left_Click(System::Object^ sender, System::EventArgs^ e) {
		
		count_up--;
		switch (count_up) {
		case 0:
			break;
		case 1:
			textBox3->Text = "10";
			break;
		case 2:
			textBox3->Text = "20";
			break;
		case 3:
			textBox3->Text = "30";
			break;
		case 4:
			textBox3->Text = "40";
			break;
		default:
			textBox3->Text = "0";
			count_up = 5;
			break;
		}
	}
private: System::Void textBox3_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	/**/
	}
private: System::Void right_MouseClick(System::Object^ sender, System::Windows::Forms::MouseEventArgs^ e) {
		
		
		switch (count_up) {
		case 0:
			break;
		case 1:
			textBox3->Text = "10";
			break;
		case 2:
			textBox3->Text = "20";
			break;
		case 3:
			textBox3->Text = "30";
			break;
		case 4:
			textBox3->Text = "40"; 
			break;
		default:
			textBox3->Text = "0";
			count_up = 0;
			break;
		}
		count_up++;
		}
};
}

